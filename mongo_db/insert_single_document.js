var mongodb = require('mongodb');

var mongoClient = mongodb.MongoClient;
var url = "mongodb://localhost:27017/nodedemo";

mongoClient.connect(url, function(err, db) {
  if (err) 
  {
	  throw err;
  }
  
  var customer = {name:"Santosh Kumar" , address: "B-222, Sector-19, NOIDA", orderdata:"Arrow Shirt"};
  
  db.collection("customers").insertOne(customer, function(error, response) {
	 if (err) {
			throw err;
	 }
	 
	console.log("1 document inserted");
    db.close();
	 
	 
  });
  
  
});