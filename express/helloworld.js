var express = require('express');
var app = express();

app.get('/', function (req, res) {
	
   // request object
   req.accepts('text/html');
   var name = req.param('name'); // ?name=tobi // this is depricated
   
   if (name === undefined) {
	 name = "";   
   }
   
   var class1 = req.param('class'); // ?name=nodejs2222&class=111
   
   
   console.log("req.query.name >>> " + req.query.name);
   console.log("req.protocol >>> " + req.protocol);
   console.log("req.path >>> " + req.path);
   console.log("req.hostname >>> " + req.hostname);
   
   
   
   res.send('Hello World ' + name + " " + class1);
   
})

var server = app.listen(8081, function () {
  console.log("Listening on http://127.0.0.1:8081/");
});