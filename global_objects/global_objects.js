console.log("It prints the file name with absolute path : " +  __filename ); // The __filename represents the filename of the code being executed.

console.log("It prints the directory name with absolute path : " +  __dirname );

//setTimeout example
function printHello(){
   console.log( "\n Hello, World!");
}
// Now call above function after 5 seconds
//setTimeout(printHello, 5000);


// Now call above function after 2 seconds
setInterval(printHello, 5000);
