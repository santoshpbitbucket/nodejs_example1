var http = require("http");
 
var responseWriter = function (request, response) { 
    response.writeHead(200, {'Content-Type' : 'text/plain'});
    response.end('Hello World !!!!');
}
 
var server = http.createServer(responseWriter).listen(8000);

server.on('listening', function() {
    console.log("server listening .....write code to perform initial loading");
});


server.on('connection', function() {
    console.log("connection established");
});
 
console.log("server started");