var events = require('events');

var eventEmitter = new events.EventEmitter();

//Create an event handler:
var myEventHandler = function () {
  console.log('Hi from the event handler');
}

//Assign the eventhandler to an event:
eventEmitter.on('hi', myEventHandler);

//Fire the 'scream' event:
eventEmitter.emit('hi');
